# -*- coding: utf-8

from math import log
from math import log10
from math import exp
from math import floor
from math import ceil
from numbers import Number

class Measure(object):
        def __init__(self, value = 0.0, precision = 0.0):
                # Metodo horrivel para separar a base do expoente
                [s_prec_base, s_prec_exp] = ('%E' % precision).split('E')
                i_prec_base = int(round(float(s_prec_base),0))
                i_prec_exp  = int(s_prec_exp)

                if (i_prec_base == 10): i_prec_base, i_prec_exp = 1, i_prec_exp + 1

                # Iniciando o processo de formatacao da medida
                if i_prec_exp > 0:
                        res_value = int(round(value, -i_prec_exp))
                else:
                        res_value = round(value, -i_prec_exp)
                res_precision = i_prec_base * 10 ** i_prec_exp

                self.value, self.precision = res_value, res_precision

        def __add__(self, other):
                if isinstance(other, Number):
                        return Measure(self.value + other, self.precision)
                else:
                        return Measure(self.value + other.value, self.precision + other.precision)
        __radd__ = __add__

        def __sub__(self, other):
                if isinstance(other, Number):
                        return Measure(self.value - other, self.precision)
                else:
                        return Measure(self.value - other.value, self.precision + other.precision)
        __rsub__ = __sub__

        def __mul__(self, other):
                if isinstance(other, Number):
                        return Measure(self.value * other, self.precision * abs(other))
                else:
                        return Measure(self.value * other.value, self.precision * other.value + self.value * other.precision)
        __rmul__ = __mul__
        
        def __div__(self, other):
                if isinstance(other, Number):
                        return Measure(self.value / other, self.precision / abs(other))
                else:
                        return Measure(self.value / other.value, (self.precision * other.value + self.value * other.precision) / other.value ** 2)

        # Compatibilidade com o Python 3
        __truediv__ = __div__
        __floordiv__ = __div__

        def __rdiv__(self, other):
                if isinstance(other, Number):
                        return Measure(other / self.value, abs(other) * self.precision / self.value ** 2)
                else:
                        return other.__div__(self)

        def __pow__(self, other):
                if isinstance(other, Number):
                        return Measure(self.value ** other, abs(other) * self.value ** (other - 1) * self.precision)
                else:
                        return Measure(self.value ** other.value, exp(other.value * log(self.value)) * (other.precision * log(self.value) + other.value * self.precision / self.value))

        def __eq__(self, other):
                return True if abs(self.value - other.value) <= 2 * (self.precision + other.precision) else False if abs(self.value - other.value) >= 3 * (self.precision + other.precision) else None

        def __repr__(self):
                if 0 < abs(self.precision) < 1:
                    t = -int(floor(log10(abs(self.precision))))
                    return '{0:.0{2:d}f} ± {1:.0{2:d}f}'.format(self.value, self.precision, t)
                elif abs(self.precision) >= 1:
                    return '%d ± %d' % (self.value, self.precision)
                else:
                    return str(int(self.value))
